#include <cmath>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>

#include "gnuplot-iostream.h"

#define DEFAULT_FILEPATH "/home/nico/Sandbox/montecarlos/data/data.txt"
#define DEFAULT_DELIMITER ", "
#define DEFAULT_OUTPUT "output.txt"

void plot(std::vector<double> dp, std::string tittle, std::string output,
          std::string chartype);

void getDataFromFile(const std::string fn, std::vector<double> *pos_x,
                     std::vector<double> *pos_y);

double getAverage(std::vector<double> data);

double getVariance(std::vector<double> data, double avrg);

int main(int argc, char *argv[]) {
  const int nrolls = 10000000; // number of experiments
  double average = 0;          // media
  double s2 = 0;               // varianza
  double s;                    // desviacion
  int p[100] = {};
  unsigned long long acum[100] = {};
  std::string fp;
  std::vector<double> pos_x;
  std::vector<double> pos_y;
  std::default_random_engine generator;

  (argc <= 1) ? fp = DEFAULT_FILEPATH : fp = *(argv + 1);

  // Read file
  getDataFromFile(fp, &pos_x, &pos_y);
  // Get average of read data
  average = getAverage(pos_y);
  // Variance
  s2 = getVariance(pos_y, average);
  // Desviacion
  s = std::sqrt(s2);

  std::normal_distribution<double> distribution(average, s);

  for (int i = 0; i < nrolls; ++i) {
    double number;
    do {
      number = distribution(generator);
    } while (number < 0);

    ++p[int(number)];
  }

  for (int i = 0; i < 100; ++i) {
    for (int j = 0; j <= i; ++j) {
      acum[i] += p[j];
    }
  }

  std::vector<double> dp;
  std::vector<double> dacum;
  double tmp;
  for (int i = 0; i < 100; ++i) {
    tmp = static_cast<double>(*(p + i)) / static_cast<double>(nrolls) * 100;
    dp.push_back(tmp);
    tmp = static_cast<double>(*(acum + i)) / static_cast<double>(nrolls) * 100;
    dacum.push_back(tmp);
  }

  // Normal distribution
  plot(dp, "Distribución normal", "Distribucion.png", "l");

  // Accumulated
  plot(dacum, "Distribución acumulada", "Acumulada.png", "boxes");

  std::fstream myfile;
  myfile.open(DEFAULT_OUTPUT, std::ios::out | std::ios::trunc);

  if (!myfile.is_open()) {
    std::cout << "ERROR OPENING FILE\n";
    return 1;
  }

  pos_x = {};
  pos_y = {};
  myfile << "# Día, Visitas\n";
  for (int i = 0; i < 100; ++i) {
    int number;
    do {
      number = distribution(generator);
    } while (number < 0);

    myfile << i + 1 << DEFAULT_DELIMITER << number << '\n';
    pos_x.push_back(i);
    pos_y.push_back(number);
  }
  myfile.close();

  // Mostrando datos:
  std::cout << "Datos Antiguos:"
            << "\n\tPromedio:" << average << "\n\tVarianza: " << s2
            << "\n\tDesviación: " << s << std::endl;

  // Get average of read data
  average = getAverage(pos_y);
  // Variance
  s2 = getVariance(pos_y, average);
  // Desviacion
  s = std::sqrt(s2);

  std::cout << "Datos nuevos generados:"
            << "\n\tPromedio:" << average << "\n\tVarianza: " << s2
            << "\n\tDesviación: " << s << std::endl;

  return 0;
}

void plot(std::vector<double> dp, std::string tittle, std::string output,
          std::string chartype) {
  Gnuplot gp;
  gp << "red = '#FF0000'; skyblue = '#87CEEB'\n";
  gp << "set terminal png\n";
  gp << "set title '" << tittle << "'\n";
  gp << "set output '" << output << "'\n";
  gp << "set xrange [-2:55]\n";
  gp << "set ylabel 'Probabilidad en %'\n";
  gp << "set xlabel 'Número de visitas'\n";
  gp << "set boxwidth 1\nset style fill solid\n";
  gp << "plot '-' using 1 title '' with " << chartype << " linecolor rgb red\n";
  gp.send1d(dp);
}

void getDataFromFile(const std::string fn, std::vector<double> *pos_x,
                     std::vector<double> *pos_y) {
  std::fstream myfile;
  std::string line;
  std::string token;
  std::string delimiter(DEFAULT_DELIMITER);
  size_t pos;

  myfile.open(fn);
  if (!myfile.is_open()) {
    std::cout << "ERROR OPENING FILE\n";
    exit(1);
  }
  getline(myfile, line);
  while (getline(myfile, line)) {
    pos = line.find(delimiter);
    token = line.substr(0, pos); // token = pos
    pos_x->push_back(std::stod(token));
    line.erase(0, pos + delimiter.length());
    pos_y->push_back(std::stod(line)); // line = Data
  }
  myfile.close();
}

double getAverage(std::vector<double> data) {
  double avrg = 0;
  int num;
  num = data.size();
  for (auto i = data.begin(); i != data.end(); ++i) {
    avrg += *i;
  }
  avrg /= static_cast<double>(num);
  return avrg;
}

double getVariance(std::vector<double> data, double avrg) {
  int num;
  double s2 = 0;
  num = data.size();
  // Varianza
  for (auto i = data.begin(); i != data.end(); ++i) {
    s2 += (*i - avrg) * (*i - avrg);
  }
  s2 /= (static_cast<double>(num));
  return s2;
}
